cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-dialogs.notification",
      "file": "plugins/cordova-plugin-dialogs/www/notification.js",
      "pluginId": "cordova-plugin-dialogs",
      "merges": [
        "navigator.notification"
      ]
    },
    {
      "id": "cordova-plugin-dialogs.notification_android",
      "file": "plugins/cordova-plugin-dialogs/www/android/notification.js",
      "pluginId": "cordova-plugin-dialogs",
      "merges": [
        "navigator.notification"
      ]
    },
    {
      "id": "cordova-plugin-device.device",
      "file": "plugins/cordova-plugin-device/www/device.js",
      "pluginId": "cordova-plugin-device",
      "clobbers": [
        "device"
      ]
    },
    {
      "id": "com.xqqy.cordova.httpd.CorHttpd",
      "file": "plugins/com.xqqy.cordova.httpd/www/CorHttpd.js",
      "pluginId": "com.xqqy.cordova.httpd",
      "clobbers": [
        "cordova.plugins.CorHttpd"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.3",
    "cordova-plugin-dialogs": "2.0.3-dev",
    "cordova-plugin-device": "2.0.3",
    "com.xqqy.cordova.httpd": "0.9.2"
  };
});