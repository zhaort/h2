function uni(code) { //主进度记录
    localStorage.setItem("uni", code);
    document.getElementById("loading").style.display = "block"
    initia();
}

function clsfin(node, info) { //课程完成
    localStorage.removeItem("timeset"); //移除倒计时、上课持续
    localStorage.removeItem("clsup");
    window.clearInterval(ctd);

    document.getElementById("info_content").innerHTML = '';

    //尝试动态加载，若失败提升升级浏览器
    if (httpd) {
        console.info("本地服务器动态加载课程" + node)
        import('http://127.0.0.1:8080/' + node + '.js')
            .then(module => {
                module.clsfin_e(info);//由该模块进行处理
            })
            .catch(err => {
                console.error(err);
                dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
            });
    } else {
        console.info("远程服务器动态加载课程" + node)
        import('./module/' + node + '.js')
            .then(module => {
                module.clsfin_e(info);//由该模块进行处理
            })
            .catch(err => {
                console.error(err);
                dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
            });
    }
}


function clsshow(title, require, timeset, node, footer) { //渲染上课拟态框
    document.getElementById("info_header").innerHTML = title;
    document.getElementById("info_content").innerHTML = require + "<p id=\"CountDown\">距离结束还有" + timeset + "秒</p>";
    document.getElementById("info_footer").innerHTML = footer;
    var time = new Date;
    var clsup = {
        node: node,
        endtime: parseInt(time.getTime()) + timeset * 1000,
        title: title,
        require: require,
        footer: footer
    }
    localStorage.setItem("clsup", JSON.stringify(clsup)); //使课程可恢复
    let d = new Date();
    localStorage.setItem("timeset", parseInt(d.getTime()) + timeset * 1000)
    window.setInterval(ctdgo, 1000); //启动倒计时
    /*setTimeout(() => {
        document.getElementById("timeset").classList.remove("disabled")
    }, timeset * 1000);*/
    info.open();
}

function clsrestart() {
    try {
        var clsup = JSON.parse(localStorage.getItem("clsup"));
    } catch (err) {
        dialogAlert("哦呜，课程数据损坏！课白上了");
        localStorage.removeItem("clsup");
        location.reload();
    }
    let title = clsup["title"];
    let footer = clsup["footer"];
    let require = clsup["require"];
    localStorage.setItem("timeset", clsup["endtime"]);
    document.getElementById("info_header").innerHTML = title;
    document.getElementById("info_content").innerHTML = require + "<p id=\"CountDown\">距离结束还有好多秒</p>";
    document.getElementById("info_footer").innerHTML = footer;
    ctd = window.setInterval(ctdgo, 1000);
    info.open();
}

function clsup(node) { //上课
    if (localStorage.getItem("clsup")) {//检查是否有课程继续
        dialogAlert("RBQ需要先上完现在的课程呢！");
        clsrestart()
        return;
    }
    //尝试动态加载，若失败提升升级浏览器
    if (httpd) {
        console.info("本地服务器动态加载课程" + node)
        import('http://127.0.0.1:8080/' + node + '.js')
            .then(module => {
                module.clsup_e(info);//由该模块进行处理
            })
            .catch(err => {
                console.error(err);
                dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
            });
    } else {
        console.info("远程服务器动态加载课程" + node)
        import('./module/' + node + '.js')
            .then(module => {
                module.clsup_e(info);//由该模块进行处理
            })
            .catch(err => {
                console.error(err);
                dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
            });
    }

}

//更改分数
function alpha(d) {
    if (localStorage.getItem("alpha")) {
        localStorage.setItem("alpha", parseFloat(localStorage.getItem("alpha")) + parseFloat(d));
    } else {
        localStorage.setItem("alpha", d);
    }
}
function sissy(d) {
    if (localStorage.getItem("sissy")) {
        localStorage.setItem("sissy", parseFloat(localStorage.getItem("sissy")) + parseFloat(d));
    } else {
        localStorage.setItem("sissy", d);
    }
}
function slut(d) {
    if (localStorage.getItem("slut")) {
        localStorage.setItem("slut", parseFloat(localStorage.getItem("slut")) + parseFloat(d));
    } else {
        localStorage.setItem("slut", d);
    }
}

function money(d) {
    if (localStorage.getItem("money")) {
        localStorage.setItem("money", parseFloat(localStorage.getItem("money")) + parseFloat(d));
    } else {
        localStorage.setItem("money", d);
    }
}

function itemadd(code){//添加一个物品
    if(!localStorage.getItem("item")){
        localStorage.setItem("item",code);
    }else{
            localStorage.setItem("item",localStorage.getItem("item")+"/meow/"+code);
    }
    netload(code);

    function netload(node){
        if (httpd) {
            console.info("本地服务器动态加载buff" + node)
            import('http://127.0.0.1:8080/' + node + '.js')
                .then(module => {
                    module.add_e();//由该模块进行处理
                    dialogAlert("添加成功","成功");
                })
                .catch(err => {
                    console.error(err);
                    dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
                });
        } else {
            console.info("本地服务器动态加载buff" + node)
            import('./module/' + node + '.js')
                .then(module => {
                    module.add_e();//由该模块进行处理
                    dialogAlert("添加成功","成功");
                })
                .catch(err => {
                    console.error(err);
                    dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
                });
        }
    }
}

function itemremove(code){//移除物品
    if(!localStorage.getItem("item")){
        dialogAlert("您当前没有任何物品");
        return;
    }
    var sp=localStorage.getItem("item")
    if(sp.indexOf("/meow/"+code)>-1){//如果可以从前部删除
        var end=sp.substr(0,sp.indexOf("/meow/"+code))+(sp.indexOf("/meow/"+code)+code.length+6);//切掉那部分字符串
    }else if(sp.indexOf(code+"/meow/")>-1){//如果可以从前部删除
        var end=sp.substr(0,sp.indexOf(code+"/meow/"))+(sp.indexOf(code+"/meow/")+code.length+6);//切掉那部分字符串
    }
    if(end){
        localStorage.setItem("item",end)
        netload(code);
    }else{
        dialogAlert("移除失败")
    }

    function netload(node){
        if (httpd) {
            console.info("本地服务器动态加载buff" + node)
            import('http://127.0.0.1:8080/' + node + '.js')
                .then(module => {
                    module.remove_e();//由该模块进行处理
                })
                .catch(err => {
                    console.error(err);
                    dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
                });
        } else {
            console.info("本地服务器动态加载buff" + node)
            import('./module/' + node + '.js')
                .then(module => {
                    module.remove_e();//由该模块进行处理
                })
                .catch(err => {
                    console.error(err);
                    dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
                });
        }
    }
}

function buffload(){//加载buff信息
    if(!localStorage.getItem("onitem")){
        console.log("没有任何buff");
        return
    }
    var sp=localStorage.getItem("onitem").split("/meow/");
    var t=0;
    while (t<sp.length){
        netload("buff"+sp[t])
    }
    function netload(node){
        if (httpd) {
            console.info("本地服务器动态加载buff" + node)
            import('http://127.0.0.1:8080/' + node + '.js')
                .then(module => {
                    module.buff_e();//由该模块进行处理
                })
                .catch(err => {
                    console.error(err);
                    dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
                });
        } else {
            console.info("本地服务器动态加载buff" + node)
            import('./module/' + node + '.js')
                .then(module => {
                    module.buff_e();//由该模块进行处理
                })
                .catch(err => {
                    console.error(err);
                    dialogAlert("动态加载失败，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge");
                });
        }
    }
}